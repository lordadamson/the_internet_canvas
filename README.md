# The Internet Canvas

A canvas on the browser where people can go type and draw stuff, and it would synchronize in real time.

Server is written in C, implements web sockets.
Client is javascript on the browser, uses the HTML5 canvas.

<img src="Screenshot_2024-06-23_at_1.01.56_AM.png">