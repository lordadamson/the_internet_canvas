import {canvas_render_png} from "./canvas.js"

var ws = new WebSocket('ws://localhost:4567/');

ws.addEventListener('error',   _on_error);
ws.addEventListener('open',    _on_open);
ws.addEventListener('message', _on_message);

export function websocket_send_png(image_data_url, lx, ly, gx, gy)
{
	var message = {
		img: image_data_url,
		localx: lx,
		localy: ly,
		globalx: gx,
		globaly: gy
	};

	ws.send(JSON.stringify(message));
}

function _on_error(message)
{
	console.log(message);
}

function _on_open(message)
{
	console.log(message);
}

function _on_message(message)
{
	var data = JSON.parse(message.data);
	canvas_render_png(data);
}