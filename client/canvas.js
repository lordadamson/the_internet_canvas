var canvas = document.getElementById('canvas');
var ctx = canvas.getContext("2d");

export function canvas_render_png(message)
{
	var image = new Image;
	image.src = message.img;

	image.onload = function (){
		ctx.drawImage(image, message.localx, message.localy);
	};
}