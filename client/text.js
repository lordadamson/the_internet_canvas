import {websocket_send_png} from './websocket.js';

window.addEventListener('keypress', _on_keypress, false);

var canvas = document.getElementById('canvas');
var ctx = canvas.getContext("2d");
var font = "14px monospace";

var text_x = 0;
var text_y = 0;

var mouse_x = 0;
var mouse_y = 0;

canvas.addEventListener('mousemove', _on_mousemove, false);

function _on_keypress(e)
{
	e.preventDefault();

	if(e.keyCode == 13)
	{
		text_y += 14;
		text_x = mouse_x;
		return;
	}

	ctx.font = font;
	ctx.fillText(e.key, text_x, text_y);
	text_x += 10;

	websocket_send_text(e.key, text_x, text_y);
}

function get_mouse_pos(canvas, e)
{
	var rect = canvas.getBoundingClientRect();
	return {
	  x: e.clientX - rect.left,
	  y: e.clientY - rect.top
	};
}

function _on_mousemove(e)
{
	e.preventDefault();
	
	var mousePos = get_mouse_pos(canvas, e);
	mouse_x = mousePos.x;
	mouse_y = mousePos.y;
	text_x = mousePos.x;
	text_y = mousePos.y;
}