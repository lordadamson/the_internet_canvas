import {websocket_send_png} from './websocket.js';

var canvas = document.getElementById('canvas');
var ctx = canvas.getContext("2d");
canvas.width  = 1024;
canvas.height = 768;
ctx.strokeStyle = "#000000";
ctx.lineJoin = "round";
ctx.lineWidth = 5;


function stroke_render(stroke, canvas_ctx)
{
    if(stroke.length < 2)
    {
        return;
    }

    canvas_ctx.beginPath();

    canvas_ctx.moveTo(stroke[0].x, stroke[0].y);
    canvas_ctx.stroke();

    for(var i = 1; i < stroke.length; i++)
    {
        canvas_ctx.lineTo(stroke[i].x, stroke[i].y);
        canvas_ctx.stroke();
    }

    canvas_ctx.closePath();
}

export function freehand_stroke_render(stroke)
{
    stroke_render(stroke, ctx);
}

var x1 = 0;
var y1 = 0;
var x2 = 0;
var y2 = 0;

var painting = false;
var stroke = []

function draw_new_point()
{
    ctx.lineTo(x2, y2);
    ctx.stroke();
}

function on_down(x, y)
{
    painting = true;
    x1 = x;
    y1 = y;
    x2 = x1;
    y2 = y1;

    if (painting)
    {
        ctx.beginPath();
        ctx.moveTo(x1, y1);
        ctx.stroke();
    }
}

function on_move(newx, newy)
{
    x1 = x2;
    y1 = y2;
    x2 = newx;
    y2 = newy;

    if (painting)
    {
        draw_new_point();
        stroke.push({x: x2, y: y2});
    }
}

function get_stroke_extremes(stroke)
{
    var extreme_points = [
        {x: 9999999, y:999999},
        {x: 0, y: 0}
    ];

    stroke.forEach(point => {
        if(point.x < extreme_points[0].x)
        {
            extreme_points[0].x = point.x;
        }
        if(point.x > extreme_points[1].x)
        {
            extreme_points[1].x = point.x;
        }
        if(point.y < extreme_points[0].y)
        {
            extreme_points[0].y = point.y;
        }
        if(point.y > extreme_points[1].y)
        {
            extreme_points[1].y = point.y;
        }
    });

    return extreme_points;
}

function _on_canvas_blob_ready(blob)
{
    
}

function stroke_move_to_top_left(stroke, smallest_point)
{
    var output_stroke = [];

    stroke.forEach(point => {
        output_stroke.push({x: point.x - smallest_point.x, y: point.y - smallest_point.y});
    });

    return output_stroke;
}

function send_stroke(stroke)
{
    var extreme_points = get_stroke_extremes(stroke);

    var x = extreme_points[0].x - ctx.lineWidth - 1;
    var y = extreme_points[0].y - ctx.lineWidth - 1;
    var w = extreme_points[1].x - x + ctx.lineWidth + 1;
    var h = extreme_points[1].y - y + ctx.lineWidth + 1;

    var offscreen_canvas = document.createElement("canvas");
    var offscreen_ctx = offscreen_canvas.getContext("2d");
    offscreen_canvas.width  = w;
    offscreen_canvas.height = h;
    offscreen_ctx.strokeStyle = ctx.strokeStyle;
    offscreen_ctx.lineJoin = ctx.lineJoin;
    offscreen_ctx.lineWidth = ctx.lineWidth;
    
    stroke = stroke_move_to_top_left(stroke, {x: x, y: y});
    stroke_render(stroke, offscreen_ctx);

    var image_data_url = offscreen_canvas.toDataURL("image/png");

    websocket_send_png(image_data_url, x, y, 0, 0);
}

function _on_brush_btn_click()
{
    ctx.strokeStyle = "#000";
}

function _on_eraser_btn_click()
{
    ctx.strokeStyle = "#fff";
}

// handles both mouse and touch
function _on_mouseup(e)
{
    ctx.closePath();
    send_stroke(stroke);
    stroke = [];
    painting = false;
}

function _on_mousedown(e)
{
    on_down(e.pageX - canvas.offsetLeft, e.pageY - canvas.offsetTop);
}

function _on_touchstart(e)
{
    on_down(e.touches[0].pageX - canvas.offsetLeft, e.touches[0].pageY - canvas.offsetTop);
}

function _on_mousemove(e)
{
    on_move(e.pageX - canvas.offsetLeft, e.pageY - canvas.offsetTop);
}

function _on_touchmove(e)
{
    on_move(e.touches[0].pageX - canvas.offsetLeft, e.touches[0].pageY - canvas.offsetTop);
}

canvas.addEventListener('mouseup',    _on_mouseup);
canvas.addEventListener('mousemove',  _on_mousemove);
canvas.addEventListener('mousedown',  _on_mousedown);
canvas.addEventListener('touchstart', _on_touchstart);
canvas.addEventListener('touchmove',  _on_touchmove);
canvas.addEventListener('touchend',   _on_mouseup);

document.getElementById("eraser_btn").addEventListener('click', _on_eraser_btn_click);
document.getElementById("brush_btn").addEventListener('click',  _on_brush_btn_click);