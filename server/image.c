#include "image.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>

#define CANVAS_WIDTH    1024
#define CANVAS_HEIGHT   768
#define CANVAS_CHANNELS 4
#define CANVAS_BYTE_SIZE CANVAS_WIDTH * CANVAS_HEIGHT * CANVAS_CHANNELS

static const uint8_t empty_canvas[CANVAS_BYTE_SIZE];

uint8_t*
pixel(struct Image self, size_t x, size_t y)
{
	return &self.data[self.c * (self.w * y + x)];
}

struct Image
image_load_from_png(const char* png, size_t len, int* error)
{
	struct Image img = {0};

	img.data = stbi_load_from_memory((const uint8_t*)png, (int)len,
									 (int*)&img.w, (int*)&img.h, (int*)&img.c, 4);

	if(img.data == NULL || img.c != 4)
	{
		*error = 1;
	}

	assert(img.c == 4);

	return img;
}

static void
image_file_delete(const char* path)
{
	if(!remove(path))
	{
		abort();
	}
}

static void
image_file_create(const char* path)
{
	int fd = open(path, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
	write(fd, empty_canvas, CANVAS_BYTE_SIZE);
	close(fd);
}

void
image_persist_to_canvas(const char* canvas_path, struct Image img, size_t x, size_t y)
{
	if(access(canvas_path, F_OK ) == -1)
	{
		image_file_create(canvas_path);
	}

	int fd = open(canvas_path, O_RDWR, S_IRUSR | S_IWUSR);

	struct stat sb;

	if(fstat(fd, &sb) != 0)
	{
		abort();
	}

	if(sb.st_size != CANVAS_BYTE_SIZE)
	{
		close(fd);
		image_file_delete(canvas_path);
		image_persist_to_canvas(canvas_path, img, x, y);
		return;
	}

	struct Image canvas;
	canvas.data = mmap(NULL, (size_t)sb.st_size,
					   PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	canvas.w = CANVAS_WIDTH;
	canvas.h = CANVAS_HEIGHT;
	canvas.c = CANVAS_CHANNELS;

	const size_t max_w = x + img.w;
	const size_t max_h = y + img.h;

	size_t iy = 0;
	for(size_t cy = y; cy < max_h; cy++, iy++)
	{
		size_t ix = 0;
		for(size_t cx = x; cx < max_w; cx++, ix++)
		{
			uint8_t* pi = pixel(img, ix, iy);

			if(pi[3] == 0)
			{
				continue;
			}

			uint8_t* pc = pixel(canvas, cx, cy);

			memcpy(pc, pi, 4);
		}
	}
	image_write_png(img, "/home/adam/img.png");
	image_write_png(canvas, "/home/adam/canvas.png");

	close(fd);
}

void
image_free(struct Image img)
{
	if(img.data)
	{
		free(img.data);
	}
}

struct Image
image_load_from_canvas(const char* canvas_path)
{
	if(access(canvas_path, F_OK ) == -1)
	{
		image_file_create(canvas_path);
	}

	int fd = open(canvas_path, O_RDWR, S_IRUSR | S_IWUSR);

	struct stat sb;

	if(fstat(fd, &sb) != 0)
	{
		abort();
	}

	if(sb.st_size != CANVAS_BYTE_SIZE)
	{
		close(fd);
		image_file_delete(canvas_path);
		return image_load_from_canvas(canvas_path);
	}

	struct Image output;

	output.data = malloc(CANVAS_BYTE_SIZE);
	output.w = CANVAS_WIDTH;
	output.h = CANVAS_HEIGHT;
	output.c = CANVAS_CHANNELS;

	char* canvas_data = mmap(NULL, (size_t)sb.st_size,
					   PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

	memcpy(output.data, canvas_data, CANVAS_BYTE_SIZE);

	return output;
}

struct PNGWriteCtx
{
	struct PNG png;
	size_t read_so_far;
};

static void
write_func(void *context, void *data, int size)
{
	struct PNGWriteCtx* ctx = (struct PNGWriteCtx*)context;

	memcpy(ctx->png.data + ctx->read_so_far, data, (size_t)size);
	ctx->read_so_far += (size_t)size;
}

struct PNG
image_to_png(struct Image img)
{
	struct PNGWriteCtx ctx;

	ctx.png.data = malloc(CANVAS_BYTE_SIZE);
	ctx.png.len = CANVAS_BYTE_SIZE;
	ctx.read_so_far = 0;

	stbi_write_png_to_func(write_func, &ctx,
						   (int)img.w, (int)img.h, (int)img.c, img.data, 0);

	return ctx.png;
}

void
image_write_png(struct Image img, const char* path)
{
	stbi_write_png(path, (int)img.w, (int)img.h, (int)img.c, img.data, 0);
}
