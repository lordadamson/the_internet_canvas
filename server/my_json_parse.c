#include "my_json_parse.h"

#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>

struct Context
{
	const char* json;
	size_t json_len;
	size_t idx;
};

// expect ows or nl
// ows = optional white space
// nl  = new line
static void
expect_ows_nl(struct Context* ctx)
{
	for(; ctx->idx < ctx->json_len; ctx->idx++)
	{
		if(ctx->json[ctx->idx] == ' '  ||
		   ctx->json[ctx->idx] == '\t' ||
		   ctx->json[ctx->idx] == '\r' ||
		   ctx->json[ctx->idx] == '\n')
		{
			continue;
		}

		break;
	}
}

/// guarentees that ctx will not be changed in case we found nothing
static int
expect(struct Context* ctx, const char* what)
{
	size_t idx = ctx->idx;

	size_t what_len = strlen(what);

	if(what_len > ctx->json_len - ctx->idx)
	{
		return 1;
	}

	for(size_t i = 0; i < what_len && ctx->idx < ctx->json_len; i++, idx++)
	{
		if(what[i] == ctx->json[idx])
		{
			continue;
		}

		return 1;
	}

	ctx->idx = idx;
	return 0;
}

static int
expect_key(struct Context* ctx, const char* key)
{
	int exit_status = expect(ctx, "\"");

	if(exit_status != 0)
	{
		return exit_status;
	}

	exit_status = expect(ctx, key);

	if(exit_status != 0)
	{
		return exit_status;
	}

	exit_status = expect(ctx, "\"");

	if(exit_status != 0)
	{
		return exit_status;
	}

	return expect(ctx, ":");
}

/// returns a reference to the string you want in the json along with its len
/// you should probably copy that string
static int
fetch_str(struct Context* ctx, const char** value, size_t* len)
{
	assert(len);

	int exit_status = expect(ctx, "\"");

	if(exit_status != 0)
	{
		return exit_status;
	}

	*len = 0;
	*value = ctx->json + ctx->idx;

	for(; ctx->idx < ctx->json_len; ctx->idx++, (*len)++)
	{
		if(ctx->json[ctx->idx] == '\\')
		{
			ctx->idx++;
			(*len)++;
			continue;
		}

		if(ctx->json[ctx->idx] == '\"')
		{
			ctx->idx++;
			return 0;
		}
	}

	return 1;
}

static int
my_atoul(const char* str, size_t len, size_t* output)
{
	struct Context ctx = { .json = str, .json_len = len, .idx = 0};

	int exit_status = expect(&ctx, "-");

	if(exit_status == 0)
	{
		return 1;
	}

	size_t res = 0;

	for (size_t i = 0; i < len; i++)
	{
		if(!isdigit(str[i]))
		{
			return 1;
		}

		res = res * 10 + (size_t)str[i] - '0';
	}

	*output = res;
	return 0;
}

static int
fetch_size_t(struct Context* ctx, size_t* value)
{
	assert(value);

	size_t size_t_len = 0;
	const char* size_t_as_str = ctx->json + ctx->idx;

	for(; ctx->idx < ctx->json_len; ctx->idx++, size_t_len++)
	{
		if(ctx->json[ctx->idx] == ' '  ||
		   ctx->json[ctx->idx] == ','  ||
		   ctx->json[ctx->idx] == '\t' ||
		   ctx->json[ctx->idx] == '\r' ||
		   ctx->json[ctx->idx] == '\n' ||
		   ctx->json[ctx->idx] == '}')
		{
			break;
		}
	}

	return my_atoul(size_t_as_str, size_t_len, value);
}

static int
parse_as_size_t(struct Context* ctx, const char* key, size_t* value)
{
	int exit_status = 0;

	expect_ows_nl(ctx);

	exit_status = expect_key(ctx, key);

	if(exit_status != 0)
	{
		return exit_status;
	}

	expect_ows_nl(ctx);

	exit_status = fetch_size_t(ctx, value);

	if(exit_status != 0)
	{
		return exit_status;
	}

	expect_ows_nl(ctx);

	return 0;
}

static int
parse_as_string(struct Context* ctx, const char* key, char** value)
{
	int exit_status = 0;

	expect_ows_nl(ctx);

	exit_status = expect_key(ctx, key);

	if(exit_status != 0)
	{
		return exit_status;
	}

	expect_ows_nl(ctx);

	const char* value_in_json;
	size_t value_len;

	exit_status = fetch_str(ctx, &value_in_json, &value_len);

	if(exit_status != 0)
	{
		return exit_status;
	}

	*value = malloc(value_len + 1);

	memcpy(*value, value_in_json, value_len);

	*(*(value) + value_len) = '\0';

	expect_ows_nl(ctx);

	return 0;
}

struct JsonContent
my_json_parse(const char* json, size_t len, int* exit_status)
{
	struct JsonContent output = {0};

	if(exit_status == NULL)
	{
		// you can't pass exit_status as null
		// fix your code
		abort();
	}

	if(len < 80)
	{
		*exit_status = 1;
		return output;
	}

	struct Context ctx = {json, len, 0};

	*exit_status = expect(&ctx, "{");

	if(*exit_status != 0)
	{
		return output;
	}

	*exit_status = parse_as_string(&ctx, "img", &output.img);

	if(*exit_status != 0)
	{
		return output;
	}

	*exit_status = expect(&ctx, ",");

	if(*exit_status != 0)
	{
		return output;
	}

	*exit_status = parse_as_size_t(&ctx, "localx", &output.localx);

	if(*exit_status != 0)
	{
		return output;
	}

	*exit_status = expect(&ctx, ",");

	if(*exit_status != 0)
	{
		return output;
	}

	*exit_status = parse_as_size_t(&ctx, "localy", &output.localy);

	if(*exit_status != 0)
	{
		return output;
	}

	*exit_status = expect(&ctx, ",");

	if(*exit_status != 0)
	{
		return output;
	}

	*exit_status = parse_as_size_t(&ctx, "globalx", &output.globalx);

	if(*exit_status != 0)
	{
		return output;
	}

	*exit_status = expect(&ctx, ",");

	if(*exit_status != 0)
	{
		return output;
	}

	*exit_status = parse_as_size_t(&ctx, "globaly", &output.globaly);

	return output;
}

/* reverse:  reverse string s in place */
static void
reverse(char s[])
{
	char c;

	size_t i = 0;
	size_t j = strlen(s)-1;

	for (; i < j; i++, j--)
	{
		c = s[i];
		s[i] = s[j];
		s[j] = c;
	}
}

static void
itoa(size_t n, char s[])
 {
	 int i = 0;

	 /* generate digits in reverse order */
	 do
	 {
		 s[i++] = n % 10 + '0'; /* get next digit */
	 }
	 while ((n /= 10) > 0); /* delete it */

	 s[i] = '\0';
	 reverse(s);
 }

char*
my_json_make(struct JsonContent json_c)
{
	char localx[265];
	char localy[265];
	char globalx[265];
	char globaly[265];

	itoa(json_c.localx, localx);
	itoa(json_c.localy, localy);
	itoa(json_c.globalx, globalx);
	itoa(json_c.globaly, globaly);

	size_t size = strlen("{\"img\":\"\",\"localx\":,\"localy\":,\"globalx\":,\"globaly\":}");
	size += strlen(json_c.img);
	size += strlen(localx);
	size += strlen(localy);
	size += strlen(globalx);
	size += strlen(globaly);
	size += 1; // null terminator

	char* output = malloc(size);

	size_t offset = strlen("{\"img\":\"");
	memcpy(output, "{\"img\":\"", strlen("{\"img\":\""));

	memcpy(output + offset, json_c.img, strlen(json_c.img));
	offset += strlen(json_c.img);

	memcpy(output + offset, "\",\"localx\":", strlen("\",\"localx\":"));
	offset += strlen("\",\"localx\":");

	memcpy(output + offset, localx, strlen(localx));
	offset += strlen(localx);

	memcpy(output + offset, ",\"localy\":", strlen(",\"localy\":"));
	offset += strlen(",\"localy\":");

	memcpy(output + offset, localy, strlen(localy));
	offset += strlen(localy);

	memcpy(output + offset, ",\"globalx\":", strlen(",\"globalx\":"));
	offset += strlen(",\"globalx\":");

	memcpy(output + offset, globalx, strlen(globalx));
	offset += strlen(globalx);

	memcpy(output + offset, ",\"globaly\":", strlen(",\"globaly\":"));
	offset += strlen(",\"globaly\":");

	memcpy(output + offset, globaly, strlen(globaly));
	offset += strlen(globaly);

	memcpy(output + offset, "}", 2);

	return output;
}
