#ifndef _MY_IMAGE_H
#define _MY_IMAGE_H

#include <stddef.h>
#include <stdint.h>

struct Image
{
	uint8_t* data;
	size_t w;
	size_t h;
	size_t c;
};

uint8_t*
pixel(struct Image, size_t x, size_t y);

struct Image
image_load_from_png(const char* png, size_t len, int* error);

struct Image
image_load_from_canvas(const char* path);

struct PNG
{
	uint8_t* data;
	size_t len;
};

/// Thou art responsible of the returned buffer.
/// Free it or bad things will happen (leaks)
struct PNG
image_to_png(struct Image);

void
image_write_png(struct Image, const char* path);

void
image_persist_to_canvas(const char* canvas_path, struct Image, size_t x, size_t y);

void
image_free(struct Image);

#endif
