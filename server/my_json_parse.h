#ifndef _MY_JSON_PARSE_H
#define _MY_JSON_PARSE_H

#include <stddef.h>

struct JsonContent
{
	char* img;
	size_t localx;
	size_t localy;
	size_t globalx;
	size_t globaly;
};

struct JsonContent
my_json_parse(const char* json, size_t len, int* exit_status);

char*
my_json_make(struct JsonContent);

#endif
